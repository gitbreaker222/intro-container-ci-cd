from flask import Flask, jsonify, render_template
from emoji import emojize

app = Flask(__name__)


@app.template_filter('emojify')
def emoji_filter(emoji_shortcode: str) -> str:
    '''
    A utility function that implements the capability to transform
    emoji shortcodes into their visual representation
    '''
    return emojize(emoji_shortcode)


@app.get("/")
def index():
    return render_template("pages/index.html")


@app.get("/workshop-json")
def some_json():
    return jsonify({
        "teamparallax": "a group of experts",
        "containers": "are cool things!"
    })


if __name__ == "__main__":
    app.run(debug=True)
