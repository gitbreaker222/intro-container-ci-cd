# TPX Container & CI/CD Workshop

This Repository contains all source code an information from the Team Parallax Free Workshop about the `Containers & CI/CD` introduction with `Docker` and `GitLab CI`.

## Quick Start

In order to quickly get started using this repository follow these steps:

1. Clone the repository
2. Open your terminal and navigate to the location of the repository
3. Possible commands/tasks
   1. start application locally (requires an installation of python3 and pip3 on your host system)
      1. install dependencies
      2. start application server
   2. build application container image
   3. start local application container

## What's in the Box?

This repo contains:

1. the workshop [slides](./resources/container_ci_cd.pdf) as `pdf`
2. the source code of the small Demo-Application (Python, Flask)
   1. `src/`
3. a definition for a container image
   1. `Dockerfile`
4. a basic CI/CD Pipeline configuration for GitLab CI
   1. `.gitlab-ci.yml`, and `.gitlab/ci/templates.gitlab-ci.yml`
